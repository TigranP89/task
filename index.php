<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Registration</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <style>
        body{
            background-color: #FFFFFF;
        }
        .dropdown:hover .dropdown-menu {
            display: block;
            margin-top: 0;
        }
    </style>
</head>
<body>
    <div class="container mt-5 mb-5">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-md-6">
                <div class="card px-5 py-5">
                    <main class="form-signin">
                        
                        <form action="registration_form.php" method="POST">
                
                            <div class="form-reg">
                                <div class="form-group">            
                                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name"><br><br>
                                </div>
                                <div class="form-group">            
                                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name"><br><br>
                                </div>
                                <div class="form-group">            
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email"><br><br>
                                </div>
                                <div class="form-group">            
                                    <input type="password" class="form-control" id="pass" name="password" placeholder="Password"><br><br>
                                </div>
                                <div class="form-group">           
                                    <input type="password" class="form-control" id="repass" name="repass" placeholder="Repeat password"><br><br>
                                </div>
                                <input type="submit" value="Submit" class="btn btn-outline-success">
                            </div>
                        
                        </form>
                        
                    </main>
                </div>
            </div>

        </div>    
    </div>




    
</body>
</html>