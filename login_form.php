<?php
    
    if(empty($_POST['email']) || empty($_POST['password'])){
        exit('Email and Password are Required');
    }
    
    $servername = "localhost";
    $username = "root";
    $password = "";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=worktask", $username, $password);
         
         $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
    
    $stat = $conn->prepare('SELECT * FROM user WHERE email = :email');
    $stat->execute([':email' => $_POST['email']]);
    $result = $stat->fetchAll(PDO::FETCH_ASSOC);
    
    if(empty($result)) {
        echo "No such user with the email!";   
    }
    
    $user = array_shift($result);
    
    if($user['email'] === $_POST['email'] && $user['password'] === $_POST['password']) {
        echo "Login completed!";   
    } else {
        echo "Incorrect Email or Password!"; 
    }