<?php

    if(empty($_POST['fname']) || empty($_POST['lname']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['repass'])){
        exit('Empty field');
    }
    
    if( $_POST['password'] !== $_POST['repass']){
        exit('Password and Required password must match!');
    }

    $servername = "localhost";
    $username = "root";
    $password = "";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=worktask", $username, $password);
         
         $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
    
    $stat = $conn->prepare('INSERT INTO user (fname, lname, email, password) VALUES (:fname, :lname, :email, :password)');
    
    if ($stat) {
        $stat->execute([
            ':fname' => $_POST['fname'],
            ':lname' => $_POST['lname'],
            ':email' => $_POST['email'],
            ':password' => $_POST['password'],
        ]);
    }
    
    header('Location: login.php');
    exit();